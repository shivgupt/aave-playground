import { run, ethers, network } from "hardhat";
import { utils } from "ethers"
import AaveOracle from "@aave/protocol-v2/artifacts/contracts/misc/AaveOracle.sol/AaveOracle.json";

const AAVE_ORACLE_ADD = "0xA50ba011c48153De246E5192C8f9258A2ba79Ca9";
const DAI_ADD = "0x6B175474E89094C44Da98b954EedeAC495271d0F";
const AAVE_ORACLE_OWNER_ADD = "0xee56e2b3d491590b5b31738cc34d5232f378a8d5"

async function main() {
  await run('compile');

  // Deploy MockAggregator for DAI price
  const MockAggregator = await ethers.getContractFactory("MockAggregator");
  const mockAggregator = await MockAggregator.deploy(464781000000000);
  await mockAggregator.deployed();
  console.log("MockAggregator deployed to:", mockAggregator.address);

  // Send funds to AAVE oracle owner
  const [alice] = await ethers.getSigners();
  alice.sendTransaction({value: utils.parseUnits("2"), to: AAVE_ORACLE_OWNER_ADD});

  // Impersonate owner of Aave price oracle
  await network.provider.request({
    method: "hardhat_impersonateAccount",
    params: [AAVE_ORACLE_OWNER_ADD]}
  )
  const signer = await ethers.provider.getSigner(AAVE_ORACLE_OWNER_ADD)

  // Set DAI price to mockAggregator
  const aavePriceOracle = new ethers.Contract(AAVE_ORACLE_ADD ,AaveOracle.abi, signer);
  await aavePriceOracle.setAssetSources([DAI_ADD], [mockAggregator.address]);

  // Stop impersonating owner of Aave price oracle
  await network.provider.request({
    method: "hardhat_stopImpersonatingAccount",
    params: [AAVE_ORACLE_OWNER_ADD]}
  )
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
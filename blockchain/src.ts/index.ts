import { Wallet, providers, } from "ethers";
import {
  TxBuilderV2,
  Network,
  Market,
  formatUserSummaryData,
  formatReserves,
  valueToBigNumber,
 } from "@aave/protocol-js"
 import { getPoolReserveData, getUserReserveData } from "./utils/graphQuery";
 import { queryUserData } from "./utils/queryAPI";

require('dotenv').config();

const ethprovider_url = `https://kovan.infura.io/v3/${process.env.INFURA_KEY}`

const provider = new providers.JsonRpcProvider(ethprovider_url);
const txBuilder = new TxBuilderV2(Network.kovan, ethprovider_url);
const wallet = new Wallet(process.env.PRIVATE_KEY, provider);

console.log(wallet.address.toLowerCase());
const LINK = "0xAD5ce863aE3E4E9394Ab43d4ba0D80f419F61789";
const UNI = "0x075A36BA8846C6B6F53644fDd3bf17E5151789DC";
const DAI = "0xFf795577d9AC8bD7D90Ee22b6C1703490b6512FD";
const lendingPool = txBuilder.getLendingPool(Market.Proto);

const mintAsset = async (asset: string, symbol: string) => {
  const faucet = txBuilder.faucetService;
  let TxObject = await faucet.mint({
    userAddress: wallet.address,
    reserve: asset,
    tokenSymbol: symbol
  });

  wallet.sendTransaction(await TxObject[0].tx())
}

const getAPY = async (asset: string) => {
  const rawReservesData = (await getPoolReserveData()).reserves;

  let formatedReserveData = formatReserves(rawReservesData);

  for (let i = 0; i < formatedReserveData.length; i++) {
    if (formatedReserveData[i].symbol === asset) {
      const apy = valueToBigNumber("214593712547918960697492")
        .dividedBy(valueToBigNumber(10).pow(25))
      console.log(formatedReserveData[i].liquidityRate)
      console.log(apy.toString(10))
    }
  }
  //console.log(formatedReserveData);
}

const depositAsset = async (asset: string, amount: string) => {
  const erc20 = txBuilder.erc20Service;

  let TxObject = await lendingPool.deposit({
    user: wallet.address,
    amount,
    reserve: asset,
    onBehalfOf: wallet.address
  })

  for (let i = 0; i < TxObject.length; i++) {
    wallet.sendTransaction(await TxObject[i].tx())
  }
}

const borrowAsset = async (asset: string, amount?: string) => {

  const rawReservesData = (await getPoolReserveData()).reserves;
  const rawUserReservesData = (await getUserReserveData(wallet.address)).userReserves;

  let summary = formatUserSummaryData(
    rawReservesData,
    rawUserReservesData,
    wallet.address,
    3345,
    Math.round(new Date().getTime()/1000)
  )
  let assetPriceInEth: string
  let assetDecimal: string

  for (let i = 0; i < rawReservesData.length; i++) {
    if (rawReservesData[i].symbol === asset) {
      assetDecimal = rawReservesData[i].decimals;
      assetPriceInEth = rawReservesData[i].price.priceInEth;
      console.log(rawReservesData[i])
    }
  }
  const maxAmountToBorrow = valueToBigNumber(summary.availableBorrowsETH)
    .multipliedBy(valueToBigNumber(10).pow(assetDecimal))
    .dividedBy(assetPriceInEth) ;
  // console.log(maxAmountToBorrow.toString(10))
  // console.log(summary.availableBorrowsETH);

  //console.log(summary)
}
// queryUserData(wallet.address)

getAPY("DAI")
//borrowAsset("DAI")
//mintAsset(DAI, "DAI")
//depositAsset(DAI, "200");

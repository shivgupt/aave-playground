import { ethers, network, run } from "hardhat";
import { Contract, constants, utils } from "ethers"
import {
  borrowMax,
  depositERC20,
  initialize,
  AAVE_ORACLE,
  AAVE_ORACLE_OWNER_ADD,
  isHFBelowThreshold,
  simulate,
  liquidate,
} from "./utils";

async function main() {
  await run('compile');
  const [alice, bob] = await ethers.getSigners();

  // Setup account and contract instances
  await initialize(alice);

  // Deposit Weth in lendingPool
  await depositERC20("2", alice.address);

  // Borrow DAI from lendingPool
  await borrowMax("DAI", alice.address);

  // Simulate Debt token price change with mock oracle
  await simulate();

  if (await isHFBelowThreshold(alice.address)) {
    await liquidate(bob, alice.address);
  }
}

main()
  .then(() => console.log("Liquidated"))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
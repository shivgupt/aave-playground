import { ethers, network } from "hardhat";
import { Contract, constants, utils, BigNumber } from "ethers"

import WethAbi from "./abis/WETH.json"; 
import LendingPoolAddressesProvider from "@aave/protocol-v2/artifacts/contracts/protocol/configuration/LendingPoolAddressesProvider.sol/LendingPoolAddressesProvider.json";
import LendingPool from "@aave/protocol-v2/artifacts/contracts/protocol/lendingpool/LendingPool.sol/LendingPool.json";
import WETHGateway from "@aave/protocol-v2/artifacts/contracts/misc/WETHGateway.sol/WETHGateway.json";
import AaveOracle from "@aave/protocol-v2/artifacts/contracts/misc/AaveOracle.sol/AaveOracle.json";
import ERC20 from "@aave/protocol-v2/artifacts/contracts/interfaces/IAToken.sol/IAToken.json";

const LENDING_POOL_ADDRESS_PROVIDER = "0xB53C1a33016B2DC2fF3653530bfF1848a515c8c5"
const UNISWAP_ADAPTER = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
const WETH_GATEWAY = "0xcc9a0B7c43DC2a5F023Bb9b738E45B0Ef6B06E04"
const WETH =  "0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2";
export const AAVE_ORACLE = "0xA50ba011c48153De246E5192C8f9258A2ba79Ca9"
export const AAVE_ORACLE_OWNER_ADD = "0xee56e2b3d491590b5b31738cc34d5232f378a8d5"

const DAI = "0x6B175474E89094C44Da98b954EedeAC495271d0F";

const WAD = BigNumber.from(10).pow(18);
const LIQUIDATION_CLOSE_FACTOR_PERCENT = 5000;

let LENDING_POOL: string;
const lendingPoolAddressesProvider = new Contract(
      LENDING_POOL_ADDRESS_PROVIDER,
      LendingPoolAddressesProvider.abi,
      ethers.provider
    );
const aaveOracle = new Contract(AAVE_ORACLE, AaveOracle.abi, ethers.provider);
let weth: Contract;
let lendingPool: Contract;

export const initialize = async (signer: any) => {
  // Get LendingPool address from address provider
  LENDING_POOL = await lendingPoolAddressesProvider.getLendingPool();
  
  lendingPool = new Contract(LENDING_POOL, LendingPool.abi, signer);
  weth = new Contract(WETH, WethAbi, signer);

  // init account with weth to play with
  await weth.deposit({value: utils.parseEther("2")});

  // Send funds to AAVE oracle owner
  signer.sendTransaction({value: utils.parseUnits("2"), to: AAVE_ORACLE_OWNER_ADD});
};

export const depositERC20 = async (amount: string, onBehalf: string) => {
  // Approve lending pool to spend msg.sender's asset
  await weth.approve(LENDING_POOL, constants.MaxUint256);

  // After deposit onBehalf address receives the aToken
  await lendingPool.deposit(WETH, utils.parseEther(amount), onBehalf, 0);
};

export const borrowMax =  async (asset: string, user: string) => {
  const maxBorrow = await getAvailableToBorrow(DAI, user);

  if (maxBorrow.gt(constants.Zero)) {
    const a = await lendingPool.borrow(DAI, maxBorrow, 2, 0, user);
  }

  await getUserAccountData(user);
}

export const simulate = async () => {
  // Increase debt token price by factor of 4 to simulate liquidation
  const newAssetPriceInEth = (await aaveOracle.getAssetPrice(DAI)).mul(4);

  // Deploy MockAggregator for DAI to simulate liquidation
  const MockAggregator = await ethers.getContractFactory("MockAggregator");
  const mockAggregator = await MockAggregator.deploy(newAssetPriceInEth);
  await mockAggregator.deployed();
  console.log("MockAggregator deployed to:", mockAggregator.address);

  // Impersonate owner of Aave price oracle
  await network.provider.request({
    method: "hardhat_impersonateAccount",
    params: [AAVE_ORACLE_OWNER_ADD]}
  )
  const signer = await ethers.provider.getSigner(AAVE_ORACLE_OWNER_ADD)

  await updateOracle(signer, mockAggregator.address);

  // Stop impersonating owner of Aave price oracle
  await network.provider.request({
    method: "hardhat_stopImpersonatingAccount",
    params: [AAVE_ORACLE_OWNER_ADD]}
  )

}

export const isHFBelowThreshold = async (user: string) => {
  const { healthFactor } = await getUserAccountData(user);
  return healthFactor.lt(WAD);
}

export const liquidate = async (liquidator: any, user: string) => {
  // Deploy flash liquidation adapter
  const FlashLiquidation = await ethers.getContractFactory("FlashLiquidationAdapter")
  const flashLiquidation = await FlashLiquidation.deploy(
    LENDING_POOL_ADDRESS_PROVIDER,
    UNISWAP_ADAPTER,
    WETH
  )
  await flashLiquidation.deployed();
  console.log("FlashLiquidationAdaptor deployed to: ", flashLiquidation.address);

  const debtToClear = 3000

  const params = utils.defaultAbiCoder.encode(
    ['address', 'address', 'address', 'uint256', 'bool'],
    [WETH, DAI, user, debtToClear, false]
  )

  await flashLiquidation.connect(liquidator).requestFlashLoan(
    [DAI],
    [debtToClear],
    [0],
    params
  )
}

// Internal Utils

const getUserAccountData = async (user: string) => {
  const {
    totalCollateralETH,
    availableBorrowsETH,
    totalDebtETH,
    healthFactor
  } = await lendingPool.getUserAccountData(user);
  console.log("\n\n");
  console.log("Credit Limit (in ETH): ", utils.formatUnits(availableBorrowsETH, "ether"));
  console.log("Total Deposits (in ETH): ", utils.formatUnits(totalCollateralETH, "ether"));
  console.log("Total Debt (in ETH): ", utils.formatUnits(totalDebtETH, "ether"));
  console.log("Helath Factor: ", utils.formatUnits(healthFactor, "ether"));
  console.log("\n\n");

  return {
    totalCollateralETH,
    availableBorrowsETH,
    totalDebtETH,
    healthFactor
  }

}

const getAvailableToBorrow = async (asset: string, user: string): Promise<BigNumber> => {
  const {
    totalCollateralETH,
    availableBorrowsETH,
    totalDebtETH,
    healthFactor
  } = await getUserAccountData(user);
  if (availableBorrowsETH.lte(constants.Zero))
    return constants.Zero;

  const assetPriceInEth = await aaveOracle.getAssetPrice(asset);
  // console.log("Eth Price = ", WAD.div(assetPriceInEth).toString());

  const maxBorrow = availableBorrowsETH.mul(WAD).div(assetPriceInEth);
  console.log('Max borrow amount DAI: ', utils.formatUnits(maxBorrow, "ether"))
  return maxBorrow.sub(maxBorrow.mod(WAD));
}

const updateOracle = async (owner: any, newAggregator: string) => {
  // Set new DAI mockAggregator
  const aaveOracleOwnerImpersonation = aaveOracle.connect(owner);
  await aaveOracleOwnerImpersonation.setAssetSources([DAI], [newAggregator]);
  // const assetPriceInEth = await aaveOracle.getAssetPrice(DAI);
  // console.log("Eth Price = ", WAD.div(assetPriceInEth).toString());
}

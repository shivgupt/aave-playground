import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-ethers";
import "hardhat-watcher";
import { task } from "hardhat/config";

require("dotenv").config();

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (args, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
  console.log(process.env.ALCHEMY_KEY)
});

export default {
  solidity: {
    compilers: [
      {
        version: "0.7.3",
      },
      {
        version: "0.6.12",
      }
    ],
  },
  paths: {
    sources: "./src.sol",
    tests: "./src.ts/test/",
  },
  networks: {
    hardhat: {
      forking: {
        url: `https://eth-mainnet.alchemyapi.io/v2/${process.env.ALCHEMY_KEY}`,
        blockNumber: 12425435
      }
    }
  },
  watcher: {
    liquidation: {
      tasks: ["compile",
      { command: "test", params: { testFiles: ["./src.ts/test/liquidation.ts"] } }],
      files: ["./src.sol", "./src.ts/test"],
    }
  },
};


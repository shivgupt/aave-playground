from web3 import Web3
import json
import os
import math

w3 = Web3(Web3.HTTPProvider("http://localhost:8545"))

DAI = ("0x6B175474E89094C44Da98b954EedeAC495271d0F", "dai.json")
WETH = ("0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2", "WETH.json")
WETH_GATEWAY = ("0xcc9a0B7c43DC2a5F023Bb9b738E45B0Ef6B06E04", "wethgateway.json")
LENDING_POOL = ("0x7d2768dE32b0b80b7a3454c06BdAc94A69DDc7A9", "lendingPool.json")
LENDING_POOL_PROVIDER = ("0xB53C1a33016B2DC2fF3653530bfF1848a515c8c5", "lendingPoolAddressProvider.json")
PRICE_ORACLE = ("0xA50ba011c48153De246E5192C8f9258A2ba79Ca9", "priceOracle.json")

def loadAccounts():
  accounts = json.load(open("../temp/keys.json"))
  for acc in accounts:
    accounts[acc]["address"] = Web3.toChecksumAddress(accounts[acc]["address"])
  return accounts

def loadAbi(abi):
  return json.load(open("./abis/%s"%(abi)))

def getContractInstance(contract):
  return w3.eth.contract(contract[0], abi=loadAbi(contract[1]))

def deposit(amount, acc):
  
  # allowance = weth.functions.allowance(acc["address"], lendingPool.address).call()

  # if allowance <= 0:
  #   tx = weth.functions.approve(lendingPool.address, 100).transact({
  #     "from": acc["address"],
  #   })

  tx = wethgateway.functions.depositETH(lendingPool.address, acc["address"], 0).transact({
    "from": acc["address"],
    "value": amount
  })

def getUserAccountData(acc):
  return lendingPool.functions.getUserAccountData(acc).call()

def getAvailableToBorrow(acc, asset):
  assetPriceInEth = priceOracle.functions.getAssetPrice(asset.address).call()
  availableInEth = getUserAccountData(acc["address"])[2]
  return math.ceil(availableInEth * math.pow(10, 18) / assetPriceInEth)

def withdraw(acc):
  aWethAddress = lendingPool.functions.getReserveData(weth.address).call()[7]
  aWeth = getContractInstance((aWethAddress, "aToken.json"))
  allowance = aWeth.functions.allowance(acc["address"], lendingPool.address).call()

  if allowance <= 0:
    tx = aWeth.functions.approve(wethgateway.address, 100).transact({
      "from": acc["address"],
    })

  tx = wethgateway.functions.withdrawETH(lendingPool.address, 1, acc["address"]).transact({
    "from": acc["address"],
  })

def borrowMax(acc):
  availableToBorrow = getAvailableToBorrow(acc, dai)

  print(availableToBorrow)
  if availableToBorrow > 0:
    lendingPool.functions.borrow(dai.address, availableToBorrow, 2, 0, acc["address"]).transact({
      "from": acc["address"],
    })

def liquidate(acc, liquidator):
  collateralManagerAdd = lendingPoolAddressProvider.functions.getLendingPoolCollateralManager().call()

  allowance = dai.functions.allowance(acc["address"], lendingPool.address).call()

  if allowance <= 0:
    tx = dai.functions.approve(lendingPool.address, 20009437083).transact({
      "from": liquidator["address"],
    })

  lendingPool.functions.liquidationCall(
    weth.address,
    dai.address,
    acc["address"],
    5000,
    True
  ).transact({"from": liquidator["address"]})

  print(collateralManagerAdd)

def printAssetPrice(asset):
  assetPriceInEth = priceOracle.functions.getAssetPrice(asset.address).call()
  print(assetPriceInEth)

# main starts here
dai = getContractInstance(DAI)
weth = getContractInstance(WETH)
lendingPool = getContractInstance(LENDING_POOL)
wethgateway = getContractInstance(WETH_GATEWAY)
priceOracle = getContractInstance(PRICE_ORACLE)
lendingPoolAddressProvider = getContractInstance(LENDING_POOL_PROVIDER)
accounts = loadAccounts()

# deposit 10 ETH from alice
# deposit(10, accounts["alice"])
# deposit(30, accounts["bob"])

# borrowMax from alice
# borrowMax(accounts["alice"])
# borrowMax(accounts["bob"])

# withdraw 1 ETH from alice
# withdraw(accounts["alice"])

print (getUserAccountData(accounts["alice"]["address"]))
print (getUserAccountData(accounts["bob"]["address"]))
# printAssetPrice(dai)

# liquidate(accounts["alice"], accounts["bob"])

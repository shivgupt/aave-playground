import axios from "axios";

const subgraph_url = "https://api.thegraph.com/subgraphs/name/aave/protocol-v2-kovan"

const execute = async (query: string) => {
  const res = await axios.post(subgraph_url, {
    query,
    variable: {},
    crossdomain: true,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "json",
    }
  })
  if (res.status === 200) {
    if (res.data.errors) return res.data.errors;
    return res.data.data
  }
}

export const getAtokenAddress = async (asset: string) => {
  const query = `query{
    reserves (where: { symbol: "DAI" }) {
      aToken { id }
    }
  }`

  return (await execute(query)).reserves[0].aToken.id
}

export const getPoolReserveData = async () => {
  const query = `query{
    reserves {
      id
      underlyingAsset
      name
      symbol
      decimals
      isActive
      isFrozen
      usageAsCollateralEnabled
      borrowingEnabled
      stableBorrowRateEnabled
      baseLTVasCollateral
      optimalUtilisationRate
      averageStableRate
      stableRateSlope1
      stableRateSlope2
      baseVariableBorrowRate
      variableRateSlope1
      variableRateSlope2
      liquidityIndex
      reserveLiquidationThreshold
      variableBorrowIndex
      aToken {
        id
      }
      vToken {
        id
      }
      sToken {
        id
      }
      availableLiquidity
      stableBorrowRate
      liquidityRate
      totalPrincipalStableDebt
      totalScaledVariableDebt
      reserveLiquidationBonus
      variableBorrowRate
      price {
        priceInEth
      }
      lastUpdateTimestamp
      stableDebtLastUpdateTimestamp
      reserveFactor
      aEmissionPerSecond
      vEmissionPerSecond
      sEmissionPerSecond
      aTokenIncentivesIndex
      vTokenIncentivesIndex
      sTokenIncentivesIndex
      aIncentivesLastUpdateTimestamp
      vIncentivesLastUpdateTimestamp
      sIncentivesLastUpdateTimestamp
    }
  }`

  return execute(query)
}

export const getUserReserveData = async (user: string) => {
  const query = `{
    userReserves (where: { user: "${user.toLowerCase()}"}){
      scaledATokenBalance
      reserve {
        id
        underlyingAsset
        name
        symbol
        decimals
        liquidityRate
        reserveLiquidationBonus
        lastUpdateTimestamp
      }
      usageAsCollateralEnabledOnUser
      stableBorrowRate
      stableBorrowLastUpdateTimestamp
      principalStableDebt
      scaledVariableDebt
      variableBorrowIndex
      aTokenincentivesUserIndex
      vTokenincentivesUserIndex
      sTokenincentivesUserIndex
    }
  }`;

  return execute(query)
}
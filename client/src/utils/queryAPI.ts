import axios from "axios";

const api_url = "https://aave-api-v2.aave.com"


export const queryUserData = async (user: string) => {
  const res = await axios.get(`${api_url}/data/UserPosition?userid=0x7d12d0d36f8291e8f7adec4cf59df6cc01d0ab97`, {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "json",
    }
  })

  console.log(res.data)
}
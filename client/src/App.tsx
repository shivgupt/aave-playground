import React, { useEffect,useState } from 'react';
import { Wallet, providers, } from "ethers";
import {
  TxBuilderV2,
  Network,
  Market,
  valueToBigNumber,
 } from "@aave/protocol-js"

import './App.css';
import { depositAsset } from './utils/aave-helper';

const ethprovider_url = "http://localhost:8545"

const provider = new providers.JsonRpcProvider(ethprovider_url);
const txBuilder = new TxBuilderV2(Network.mainnet, ethprovider_url);
const lendingPool = txBuilder.getLendingPool(Market.Proto);

function App() {

  const [block, setBlock] = useState(0)

  useEffect(()=> {
    (async ()=> {
      setBlock(await provider.getBlockNumber());
      depositAsset("0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2", "20")
    })();
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        {block}
      </header>
    </div>
  );
}

export default App;
